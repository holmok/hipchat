(function (exports) {

    'use strict';

    var emoticonList = [
        'allthethings', 'android', 'areyoukiddingme', 'arrington', 'arya', 'ashton', 'atlassian',
        'awesome', 'awthanks', 'aww', 'awwyiss', 'awyeah', 'badass', 'badjokeeel', 'badpokerface',
        'badtime', 'basket', 'beer', 'bicepleft', 'bicepright', 'bitbucket', 'boom', 'borat',
        'branch', 'bumble', 'bunny', 'cadbury', 'cake', 'candycorn', 'carl', 'caruso', 'catchemall',
        'ceilingcat', 'celeryman', 'cereal', 'cerealspit', 'challengeaccepted', 'chef', 'chewie',
        'chocobunny', 'chompy', 'chucknorris', 'clarence', 'coffee', 'confluence', 'content',
        'continue', 'cookie', 'cornelius', 'corpsethumb', 'daenerys', 'dance', 'dealwithit', 'derp',
        'disappear', 'disapproval', 'doge', 'doh', 'donotwant', 'dosequis', 'downvote', 'drevil',
        'drool', 'ducreux', 'dumb', 'evilburns', 'excellent', 'facepalm', 'failed', 'feelsbadman',
        'feelsgoodman', 'finn', 'fireworks', 'firstworldproblems', 'fonzie', 'foreveralone',
        'forscale', 'freddie', 'fry', 'ftfy', 'fu', 'fuckyeah', 'fwp', 'gangnamstyle', 'gates',
        'ghost', 'giggity', 'goldstar', 'goodnews', 'greenbeer', 'grumpycat', 'gtfo', 'haha',
        'haveaseat', 'heart', 'heygirl', 'hipchat', 'hipster', 'hodor', 'huehue', 'hugefan', 'huh',
        'ilied', 'indeed', 'iseewhatyoudidthere', 'itsatrap', 'jackie', 'jaime', 'jake', 'jira',
        'jobs', 'joffrey', 'jonsnow', 'kennypowers', 'krang', 'kwanzaa', 'lincoln', 'lol', 'lolwut',
        'megusta', 'meh', 'menorah', 'mindblown', 'motherofgod', 'ned', 'nextgendev', 'nice', 'ninja',
        'noidea', 'notbad', 'nothingtodohere', 'notit', 'notsureif', 'notsureifgusta', 'obama',
        'ohcrap', 'ohgodwhy', 'ohmy', 'okay', 'omg', 'orly', 'paddlin', 'pbr', 'philosoraptor',
        'pingpong', 'pirate', 'pokerface', 'poo', 'present', 'pumpkin', 'rageguy', 'rainicorn',
        'rebeccablack', 'reddit', 'rockon', 'romney', 'rudolph', 'sadpanda', 'sadtroll', 'salute',
        'samuel', 'santa', 'sap', 'scumbag', 'seomoz', 'shamrock', 'shrug', 'skyrim', 'standup',
        'stare', 'stash', 'success', 'successful', 'sweetjesus', 'tableflip', 'taco', 'taft', 'tea',
        'thatthing', 'theyregreat', 'toodamnhigh', 'tree', 'troll', 'truestory', 'trump', 'turkey',
        'twss', 'tyrion', 'tywin', 'unacceptable', 'unknown', 'upvote', 'vote', 'waiting', 'washington',
        'wat', 'whoa', 'whynotboth', 'wtf', 'yey', 'yodawg', 'youdontsay', 'yougotitdude', 'yuno',
        'zoidberg'];

    var emotionRx = /(^|\b|\s)\(([a-z]{1,15})\)/g;
    var mentionsRx = /(^|\s|[^\w\u00C0-\u024F\u0370-\u0527])@([\w\u00C0-\u024F\u0370-\u0527]+)(?=$|\s|[^\w\u00C0-\u024F\u0370-\u0527])(?!@)/g;
    var urlRx = /(^|\b|\s)(https?:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\b[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)[^\.\s\b]*($|\b|\s)/g;
    var trailingDot = /\.$/;
    var yqlTimeout = 5000;

    var getTitle = function (link, cb) {

        var yql = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D%22" +
            encodeURIComponent(link.url) +
            "%22%20and%0A%20%20%20%20%20%20xpath%3D'%2F%2Ftitle'&format=json&callback=?";
        $.jsonp(
            {
                url: yql,
                timeout: yqlTimeout,
                success: function (json) {
                    console.log(json);
                    var title;
                    if (json && json.query && json.query.results && json.query.results.title) {
                        link.title = json.query.results.title;
                    }
                    return cb(null, link);
                },
                error: function () {
                    //quietly fail, swallow it.
                    return cb(null, link);
                }

            });
    };

    var findMatches = function (rx, str) {
        var output = [];
        var match = null;
        while (match = rx.exec(str)) {
            output.push(match[2]);
        }
        return output;
    };

    var findMentions = function (message) {
        return findMatches(mentionsRx, message);
    };

    var findEmoticons = function (message) {
        var matches = findMatches(emotionRx, message.toLocaleLowerCase());
        return _.intersection(matches, emoticonList);
    };

    var findLinks = function (message, cb) {
        var links = findMatches(urlRx, message).map(function (url) {
            return {url: url.replace(trailingDot, '')};
        });

        async.map(links, getTitle, function (err, results) {
            return cb(null, results);
        });

    };

    var parser = {};
    parser.process = function (message, cb) {

        var mentions = findMentions(message);
        var emoticons = findEmoticons(message);
        findLinks(message, function (err, links) {

            if (err) {
                return cb(err);
            } else {
                var output = {};
                if (mentions.length > 0) {
                    output.mentions = mentions;
                }
                if (emoticons.length > 0) {
                    output.emoticons = emoticons;
                }
                if (links.length > 0) {
                    output.links = links;
                }
                return cb(null, output);
            }
        });
    };

    exports.MessageParser = parser;

}(this));