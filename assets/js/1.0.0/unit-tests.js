(function () {

    'use strict';


    var should = chai.should();
    mocha.setup('bdd');

    describe('MessageParse', function () {

        describe('process links', function () {

            var jsonp;

            beforeEach(function () {
                jsonp = sinon.stub($, 'jsonp', function (options) {
                    options.success({});
                });
            });

            afterEach(function () {
                jsonp.restore();
            });

            it('should work by itself "http://hello.com".', function (done) {

                var message = 'http://hello.com';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com');
                    done();
                });

            });

            it('should strip trailing period "http://hello.com.".', function (done) {

                var message = 'http://hello.com.';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com');
                    done();
                });

            });

            it('should work on one link at start of the message "http://hello.com is url.".', function (done) {

                var message = 'http://hello.com is url.';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com');
                    done();
                });

            });

            it('should work on one link at end of the message "url is http://hello.com".', function (done) {

                var message = 'url is http://hello.com';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com');
                    done();
                });

            });

            it('should work on one link in the middle of the message "url is http://hello.com, brother".', function (done) {

                var message = 'url is http://hello.com, brother';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com');
                    done();
                });

            });

            it('should work on more then one link in the middle of the message "url is http://hello.com, http://apple.com brother".', function (done) {

                var message = 'url is http://hello.com, http://apple.com brother';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(2);
                    data.links[0].url.should.equal('http://hello.com');
                    data.links[1].url.should.equal('http://apple.com');
                    done();
                });

            });

            it('should work with a query string "url is http://hello.com?id=1, brother".', function (done) {

                var message = 'url is http://hello.com?id=1, brother';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com?id=1');

                    done();
                });

            });


            it('should work with a hash "url is http://hello.com#section, brother".', function (done) {

                var message = 'url is http://hello.com#section, brother';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com#section');

                    done();
                });

            });

            it('should work with a hash and query "url is http://hello.com?id=1#section, brother".', function (done) {

                var message = 'url is http://hello.com?id=1#section, brother';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('links').with.length(1);
                    data.links[0].url.should.equal('http://hello.com?id=1#section');

                    done();
                });

            });


        });

        describe('process emoticons', function () {
            it('should work on one emoticon at start of the message "(awesome) stuff, fella.".', function (done) {

                var message = '(awesome) stuff, fella.?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(1);
                    data.emoticons.should.include('awesome');
                    done();
                });

            });

            it('should work on one emoticon at end of the message "your stuff, fella? (awesome)".', function (done) {

                var message = 'your stuff, fella? (awesome)';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(1);
                    data.emoticons.should.include('awesome');
                    done();
                });

            });

            it('should work on one emoticon in the middle of the message "your (awesome) stuff, fella?".', function (done) {

                var message = 'your (awesome) stuff, fella?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(1);
                    data.emoticons.should.include('awesome');
                    done();
                });

            });

            it('should work on a couple emoticon in the middle of the message "your (awesome) (android), fella?".', function (done) {

                var message = 'your (awesome) (android), fella?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(2);
                    data.emoticons.should.include('awesome');
                    data.emoticons.should.include('android');
                    done();
                });

            });

            it('should normalize emoticon the message "your (Awesome) (androID), fella?".', function (done) {

                var message = 'your (Awesome) (androID), fella?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(2);
                    data.emoticons.should.include('awesome');
                    data.emoticons.should.include('android');
                    done();
                });

            });

            it('should ignore non emoticons the message "your (Awesome) (apple), fella?".', function (done) {

                var message = 'your (Awesome) (apple), fella?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('emoticons').with.length(1);
                    data.emoticons.should.include('awesome');
                    data.emoticons.should.not.include('apple');
                    done();
                });

            });

            it('should ignore stuff in urls "http://yahoo.com#(awesome)".', function (done) {

                var jsonp = sinon.stub($, 'jsonp', function (options) {
                    options.success({});
                }), message = 'http://apple.com#(awesome)';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.not.have.property('emoticons');
                    jsonp.restore();
                    done();
                });


            });

        });

        describe('process mentions', function () {
            it('should work on one mention at start of the message "@chris you around?".', function (done) {

                var message = '@chris you around?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('chris');
                    done();
                });

            });

            it('should work on one mention at end of the message "you around @chris".', function (done) {

                var message = 'you around @chris';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('chris');
                    done();
                });

            });

            it('should work on one mention wrapped in word break chars "you around "@chris"?".', function (done) {

                var message = '"you around "@chris"?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('chris');
                    done();
                });

            });

            it('should on more than one mention "you around @chris or @john or @dave?".', function (done) {

                var message = 'you around @chris or @john or @dave?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(3);
                    data.mentions.should.include('chris');
                    data.mentions.should.include('john');
                    data.mentions.should.include('dave');
                    done();
                });

            });

            it('should latin  characters "you around @dÆve?".', function (done) {

                var message = 'you around @dÆve?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('dÆve');
                    done();
                });

            });


            it('should latin extended a  characters "you around @dĀve?".', function (done) {

                var message = 'you around @dĀve?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('dĀve');
                    done();
                });

            });

            it('should latin extended b  characters "you around @davƎ?".', function (done) {

                var message = 'you around @davƎ?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('davƎ');
                    done();
                });

            });

            it('should greek characters "you around @ͽave?".', function (done) {

                var message = 'you around @ͽave?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('ͽave');
                    done();
                });

            });

            it('should coptic characters "you around @dϪve?".', function (done) {

                var message = 'you around @dϪve?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('dϪve');
                    done();
                });

            });


            it('should cyrillic characters "you around @dАve?".', function (done) {

                var message = 'you around @dАve?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('dАve');
                    done();
                });

            });

            it('should cyrillic supplement characters "you around @daԠe?".', function (done) {

                var message = 'you around @daԠe?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('daԠe');
                    done();
                });

            });

            it('should work where it might be a emoticon but is just in () "you around (@chris)?".', function (done) {

                var message = 'you around (@chris)?';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('chris');
                    done();
                });

            });

            it('should not work in an email address "my email is me@email.com".', function (done) {

                var message = 'my email is me@email.com';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.not.have.property('mentions');
                    done();
                });

            });

            it('should not work in an url "my email is http://me:password@email.com".', function (done) {

                var jsonp = sinon.stub($, 'jsonp', function (options) {
                        options.success({});
                    }),
                    message = 'my email is http://me:password@email.com';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.not.have.property('mentions');
                    jsonp.restore();
                    done();
                });

            });

            it('should work by itself "@hello".', function (done) {

                var message = '@hello';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('hello');
                    done();
                });

            });

            it('should not work with the mashed up "@hello@goodbye@apple".', function (done) {

                var message = '@hello@goodbye@apple';

                MessageParser.process(message, function (err, data) {
                    should.not.exist(err);
                    data.should.not.have.property('mentions');
                    done();
                });

            });

        });
    });

    mocha.run();

}());