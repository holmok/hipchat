(function () {

    'use strict';

    var should = chai.should();
    mocha.setup('bdd');

    describe('MessageParse', function () {

        /***
         *  examples from the email.
         */
        describe('Examples from email.', function () {

            it('should pass the first example "@chris you around?".', function (done) {
                var message = '@chris you around?';
                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('mentions').with.length(1);
                    data.mentions.should.include('chris');

                    data.should.not.have.property('links');
                    data.should.not.have.property('emoticons');

                    done();

                });
            });

            it('should pass the second example "Good morning! (megusta) (coffee)".', function (done) {
                var message ='Good morning! (megusta) (coffee)';
                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('emoticons').with.length(2);
                    data.emoticons.should.include('megusta');
                    data.emoticons.should.include('coffee');

                    data.should.not.have.property('links');
                    data.should.not.have.property('mentions');

                    done();

                });
            });

            it('should pass the third example "Olympics are starting soon; http://www.nbcolympics.com".', function (done) {

                this.timeout(5000);
                var message ='Olympics are starting soon; http://www.nbcolympics.com';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('links').with.length(1);
                    data.links[0].should.include.keys('url');
                    data.links[0].should.include.keys('title');
                    data.links[0].url.should.equal('http://www.nbcolympics.com');
                    data.links[0].title.should.be.a('string');

                    data.should.not.have.property('emoticons');
                    data.should.not.have.property('mentions');

                    done();

                });
            });

            it('should pass the fourth example  "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016".', function (done) {

                this.timeout(5000);
                var message ='@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('mentions').with.length(2);
                    data.mentions.should.include('bob');
                    data.mentions.should.include('john');

                    data.should.have.property('emoticons').with.length(1);
                    data.emoticons.should.include('success');

                    data.should.have.property('links').with.length(1);
                    data.links[0].should.include.keys('url');
                    data.links[0].should.include.keys('title');
                    data.links[0].url.should.equal('https://twitter.com/jdorfman/status/430511497475670016');
                    data.links[0].title.should.be.a('string');


                    done();

                });
            });


        });

        /***
         * known baddies
         */
        describe('Other Integration tests.', function () {



            it('should have a link with no title on redirects. "tell me more about http://rmn.com', function (done) {

                this.timeout(5000);
                var message ='tell me more about http://rmn.com';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('links').with.length(1);
                    should.not.exist(data.links[0].title);
                    data.links[0].should.include.keys('url');
                    data.links[0].url.should.equal('http://rmn.com');

                    data.should.not.have.property('emoticons');
                    data.should.not.have.property('mentions');

                    done();

                });
            });

            it('should have a link with no title on unreachable urls. "tell me more about http://thiswillnotwork"', function (done) {

                this.timeout(5000);
                var message ='tell me more about http://thiswillnotwork';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('links').with.length(1);
                    should.not.exist(data.links[0].title);
                    data.links[0].should.include.keys('url');
                    data.links[0].url.should.equal('http://thiswillnotwork');

                    data.should.not.have.property('emoticons');
                    data.should.not.have.property('mentions');

                    done();

                });
            });

            it('should have a link with title on https. "tell me more about https://www.atlassian.com/"', function (done) {

                this.timeout(5000);
                var message ='tell me more about https://www.atlassian.com/';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('links').with.length(1);
                    data.links[0].should.include.keys('title');
                    data.links[0].should.include.keys('url');
                    data.links[0].url.should.equal('https://www.atlassian.com/');

                    data.should.not.have.property('emoticons');
                    data.should.not.have.property('mentions');

                    done();

                });
            });

            it('should have a link with no title on 404. "tell me more about https://www.atlassian.com/hubbabubba"', function (done) {

                this.timeout(5000);
                var message ='tell me more about https://www.atlassian.com/hubbabubba';

                MessageParser.process(message, function (err, data) {

                    should.not.exist(err);
                    should.exist(data);
                    data.should.be.an('object');

                    data.should.have.property('links').with.length(1);
                    should.not.exist(data.links[0].title);
                    data.links[0].should.include.keys('url');
                    data.links[0].url.should.equal('https://www.atlassian.com/hubbabubba');

                    data.should.not.have.property('emoticons');
                    data.should.not.have.property('mentions');

                    done();

                });
            });


        });

    });

    mocha.run();

}());