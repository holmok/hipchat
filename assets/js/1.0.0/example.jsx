(function () {

    'use strict';

    /**
     * The result block of parse the message
     */
    var Result = React.createClass({
        render: function () {
            if (typeof this.props.value === 'object') {
                return <pre>{JSON.stringify(this.props.value, null, 2)}</pre>;
            } else {
                return <div/>;
            }
        }
    });

    /**
     * Main component to process message
     */
    var Tester = React.createClass(
            {
                getInitialState: function () {
                    return {result: undefined};
                },
                processMessage: function (e) {

                    e.preventDefault();
                    this.setState({result: undefined, mode: 'processing'});
                    var ctx = this;
                    MessageParser.process(e.target.message.value, function (err, data) {

                        if (err) {
                            alert()
                        }
                        else {
                            ctx.setState({result: data, mode: undefined});
                        }

                    });

                },
                render: function () {

                    return (
                        <div>
                            <form className="aui top-label" onSubmit={this.processMessage}>
                                <fieldset className="top-label">
                                    <div className="field-group  top-label">
                                        <label htmlFor="message">Message</label>
                                    <textarea className="textarea full-width-field" rows="2"
                                              type="text" id="message" name="message"
                                              placeholder="Type a message here...">hey @holmok (android) is (awesome). https://www.flickr.com/photos/54942802@N04/17042104022/#(apple)</textarea>
                                    </div>
                                    <div className="buttons-container">
                                        <div className="buttons">
                                            <input className="button submit" type="submit"
                                                   value={this.state.mode === 'processing' ? 'Processing...' : 'Parse Message' }
                                                   disabled={this.state.mode === 'processing'}/>

                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <Result value={this.state.result}/>
                        </div>
                    );

                }
            })
        ;

    React.render(<Tester/>, $('#example').get(0));

}());