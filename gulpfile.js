'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');
var path = require('path');
var fs = require('fs');
var env = require('gulp-util').env;
var isJS = /\.js$/i;

//load gulp tasks
fs.readdirSync(path.resolve(__dirname, "lib/gulp-tasks")).forEach(function (file) {
    if (isJS.exec(file)) {
        require("./lib/gulp-tasks/" + file);
    }
});

gulp.task('build', function (callback) {
    return runSequence('clean', ['html','jsx','js'], callback);
});

gulp.task('run', function (callback) {
    return runSequence('build', 'serve', callback);
});