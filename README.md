# HipChat Challenge

##Overview

This is a web app to demonstrate parsing messages into mentions (@bob), emoticons (awesome) and links (urls with page titles).  It has three pages.  One with a free form message box to enter messages and parse.  A page with integration tests that runs the examples from the email I got plus some edge cases. And lastly, some unit tests.

##Running the Sample

###Requirements

This runs on gulp which runs on node, so you need:

- node @ https://nodejs.org/
- gulp @ https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md 

###Instructions

1. Grab the repo:   ```$ git clone git@bitbucket.org:holmok/hipchat.git```
2. cd into it:   ```$ cd hipchat```
3. install the node stuff: ```$ npm install```
4. run the gulp command to start it:  ```$ gulp run```
5. Enjoy in your favorite browser.
6. Make changes to html, js, jsx files and as soon as you save them, your browser have those updates with the new code.  Great for writing unit tests, but avoid having the integration test page up when doing lots of changes.

##Files
In the assets folder, there are the html and JavaScript files.  The html files are pretty much self explanatory.  The JavaScript files are more interesting. 

* **example.jsx**: This is a React component (in jsx) to handle the example page.  This gets turned into plain old js in the gulp build.
* **jquery.jsonp.js**: This is a plugin I use to make the jsonp calls to YQL to get the page titles.  It offers way better error handling (like recovery and timeouts) than $.getJSON.
* **message-parser.js**: this where the magic happens it exposes a Message Parser with one public method that parses the message according to the given spec. 
* **int-tests.js**:  integration tests that run the examples from the email and some other ones.  It actually goes out, using [YQL](https://developer.yahoo.com/yql), to get the web page titles from the url.
* **unit-tests.js**: some unit tests. these stub out the YQL calls to avoid that dependancy.

##Notes
1. I was going to use require.js to handle dependancies and such.  This really wasn’t overly complicated to need to “require it”, but some good optimizations would come from building out stuff with r.js in the gulp build.  But then I would want to build out static asset management and publishing with CDN with invalidation and such… so I chose to draw the line and just went with a bunch of script tags and use the known CDNs.
2. The tests are in the browser.  This allows for testing on different browsers.  I tested on a Mac with Chrome, Safari, Firefox, and Opera.
3.  I am very Eurocentric with my mention matching. It does not work on Kanji, Arabic and etc. JavaScript’s \w is lousy, so I came up with an alternative that would be easy to expand into other character sets.
4. Ugh, the trailing periods on urls.  It is not pretty, but my solution works, and I was banging my head against the “regex wall” a little too long, so I can up with my workaround.
5. I made some assumptions that you can glean from my tests. For example, I don’t match anything in a run-on mention like @bob@chris.
6. I used YQL to get the page titles.  Browser security does not allow this client side, unless the url points to a serve that does CORS, which we can not assume.  A better solution would be to build an in house service to do this that would have caching and such.
7. I could not find an easy way to make the mocha test look pretty in AUI.  I did see in a Jira ticket (https://ecosystem.atlassian.net/browse/AUI-2765) about it but I could not find any good docs on how to implement it.

_seeing how bitbucket added the bob mention to this readme I probably made the wrong assumption on the run-on mentions, whoops_