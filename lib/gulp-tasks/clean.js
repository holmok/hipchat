'use strict';

var gulp = require('gulp');
var settings = require('../settings');
var del = require('del');
var vinylPaths = require('vinyl-paths');
var plumber = require('gulp-plumber');

// Delete result dir for a fresh build
gulp.task('clean', function () {
    return gulp.src([settings.build.html], {read: false})
        .pipe(plumber())
        .pipe(vinylPaths(del));
});