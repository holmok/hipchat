'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var settings = require('../settings');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var react = require('gulp-react');

gulp.task('jsx', function () {
    return gulp.src(settings.assets.jsx)
        .pipe(plumber())
        .pipe(react())
        .pipe(gulpif(settings.optimize, uglify(settings.options.jsx)))
        .pipe(gulp.dest(settings.build.js))
        .pipe(browserSync.reload(settings.options.browserSyncReload));
});