'use strict';

var gulp = require('gulp');
var settings = require('../settings');
var browserSync = require('browser-sync');

// Browser sync static server
gulp.task('serve', function () {

    //what to watch
    settings.watch.forEach(function(watch){
        gulp.watch(watch.source, watch.tasks);
    });

    //run server
    return browserSync(settings.options.browserSync);

});