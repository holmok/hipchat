'use strict';

var gulp = require('gulp');
var settings = require('../settings');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');

//copy over html
gulp.task('html', function () {
    return gulp.src(settings.assets.html)
        .pipe(plumber())
        .pipe(gulp.dest(settings.build.html))
        .pipe(browserSync.reload(settings.options.browserSyncReload));
});
