'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var settings = require('../settings');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var jslint = require('gulp-jslint');

gulp.task('js', function () {
    return gulp.src(settings.assets.js)
        .pipe(plumber())
        .pipe(gulpif(settings.optimize, uglify(settings.options.js)))
        .pipe(gulp.dest(settings.build.js))
        .pipe(browserSync.reload(settings.options.browserSyncReload));
});