'use strict';

var env = require('gulp-util').env;
var path = require('path');
var util = require('util');

var buildPath = util.format('build/%s/', env.result || 'local');
var serverPath = buildPath.replace(/\/$/, "");

var options = {
    browserSyncReload: {
        stream: true
    },
    browserSync: {
        server: {baseDir: serverPath, port: 5120},
        port: 3001
    },
    jsx: {
        outSourceMap: false
    }
};

var assets = {
    jsx: [
        'assets/js/**/*.jsx'
    ],
    js: [
        'assets/js/**/*.js'
    ],
    html: [
        'assets/html/**/*.html'
    ]
};

var build = {
    html: buildPath,
    js: buildPath + 'static/js'
};

var watch = [
    {source: ['assets/js/**/*.jsx'], tasks: ['jsx']},
    {source: ['assets/js/**/*.js'], tasks: ['js']},
    {source: ['assets/html/**/*.html'], tasks: ['html']}
];

var optimize = env.optimize || (env.target && env.target.toLocaleLowerCase() !== 'local');

module.exports = {
    build: build,
    assets: assets,
    options: options,
    watch: watch,
    optimize: optimize
};